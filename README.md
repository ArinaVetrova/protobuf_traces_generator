### How to build
```
cd build
cmake ..
make
```

### How to generate files fro *.proto files
```protoc --cpp_out=. proto/*.proto```
